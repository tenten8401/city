![City](https://dl.dropboxusercontent.com/u/74904588/cityphostoshop.png)
A chunk based land protection system that allows for players to set up complex claims and create civilizations.

###Downloads
- City 0.1.26 ALPHA - [Download](http://play.pixelsky-mc.com/downloads/city/City-0.1.26.jar)
- City 0.1.25 ALPHA - [Download](http://play.pixelsky-mc.com/downloads/city/City-0.1.25.jar)


###Features
- Chunk based land claiming
- Advanced claim management systems
- Player controlled plot management

For a full list of features, refer to the official [SpongePowered thread](https://forums.spongepowered.org/t/city-claim/).

###Installation
1. Put City[version].jar in /mods folder
2. Restart/start your server
3. Configure the plugin to your likings, restarting as needed.
